﻿using ServiceProviderApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceProviderApplication.Controllers
{
    public class ConsumerController : BaseController
    {
        private const string UniqueKeyErrorMessage = "The Key field must be unique";

        public ConsumerController() { }

        public ConsumerController(ProviderContext providerContext) : base(providerContext) { }

        //
        // GET: /Consumer/

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.Consumers.ToList());
        }

        //
        // GET: /Consumer/Details/5

        public ActionResult Details(int id = 0)
        {
            Consumer consumer = db.Consumers.Find(id);
            if (consumer == null)
            {
                return HttpNotFound();
            }
            return View(consumer);
        }

        //
        // GET: /Consumer/Create

        public ActionResult Create()
        {
            Consumer consumer = new Consumer();
            consumer.Key = Guid.NewGuid().ToString("N").Substring(0, 16);
            consumer.Secret = Guid.NewGuid().ToString("N").Substring(0, 16);
            return View(consumer);
        }

        //
        // POST: /Consumer/Create

        [HttpPost]
        public ActionResult Create(Consumer consumer)
        {
            if (ModelState.IsValid)
            {
                // Make sure the user did not create a non-unique key
                var match = db.Consumers.SingleOrDefault(
                    c => c.Key == consumer.Key);
                if (match != null)
                {
                    ModelState.AddModelError("Key", UniqueKeyErrorMessage);
                }
                else
                {
                    db.Consumers.Add(consumer);
                    db.SaveChanges();
                    if (string.IsNullOrEmpty(Request["ReturnURL"]))
                    {
                        return RedirectToAction("Index");
                    }
                    var uri = new UriBuilder(Request["ReturnURL"]);
                    uri.Query += "ConsumerId=" + consumer.ConsumerID;
                    return Redirect(uri.ToString());
                }
            }
            return View(consumer);
        }

        //
        // GET: /Consumer/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Consumer consumer = db.Consumers.Find(id);
            if (consumer == null)
            {
                return HttpNotFound();
            }
            return View(consumer);
        }

        //
        // POST: /Consumer/Edit/5

        [HttpPost]
        public ActionResult Edit(Consumer consumer)
        {
            if (ModelState.IsValid)
            {
                // Make sure the user did not change the Key to
                // a non-unique value
                var match = db.Consumers.SingleOrDefault(
                    c => c.Key == consumer.Key && c.ConsumerID != consumer.ConsumerID);
                if (match != null)
                {
                    ModelState.AddModelError("Key", UniqueKeyErrorMessage);
                }
                else
                {
                    db.Entry(consumer).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(consumer);
        }

        //
        // GET: /Consumer/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Consumer consumer = db.Consumers.Find(id);
            if (consumer == null)
            {
                return HttpNotFound();
            }
            return View(consumer);
        }

        //
        // POST: /Consumer/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Consumer consumer = db.Consumers.Find(id);
            db.Consumers.Remove(consumer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}