﻿using ServiceProviderApplication.Models;
using System.Web.Mvc;
using System.Web;
using Microsoft.AspNet.Identity.Owin;

namespace ServiceProviderApplication.Controllers
{
    public class BaseController : Controller
    {
        private ProviderContext _db;

        public ProviderContext db
        {
            get
            {
                return _db ?? HttpContext.GetOwinContext().Get<ProviderContext>();
            }
            set
            {
                _db = value;
            }
        }

        public BaseController()
        {
        }

        public BaseController(ProviderContext providerContext)
        {
            this._db = providerContext;
        }
    }
}