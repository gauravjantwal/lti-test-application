﻿using System;

namespace ServiceProviderApplication.LTI
{
    public class ProviderRequest
    {
        public int ProviderRequestID { get; set; }

        public string LtiRequest { get; set; }
        public DateTime Received { get; set; }
    }
}
