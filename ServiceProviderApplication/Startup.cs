﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ServiceProviderApplication.Startup))]
namespace ServiceProviderApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
