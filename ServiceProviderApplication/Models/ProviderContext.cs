﻿using Microsoft.AspNet.Identity.EntityFramework;
using ServiceProviderApplication.LTI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ServiceProviderApplication.Models
{
    public class ProviderContext : IdentityDbContext<ApplicationUser>
    {
        public ProviderContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //Database.SetInitializer<ConsumerContext>(new CreateDatabaseIfNotExists<ConsumerContext>());
            //Database.SetInitializer<ConsumerContext>(new DropCreateDatabaseIfModelChanges<ConsumerContext>());
            //Database.SetInitializer<ConsumerContext>(new DropCreateDatabaseAlways<ConsumerContext>());
            Database.SetInitializer<ProviderContext>(new ProviderContextInitializer());
        }

        public static ProviderContext Create()
        {
            return new ProviderContext();
        }

        public DbSet<Tool> Tools { get; set; }

        // From LtiLibrary
        public DbSet<Consumer> Consumers { get; set; }
        public DbSet<ProviderRequest> ProviderRequests { get; set; }
        public DbSet<Outcome> Outcomes { get; set; }
    }

    public class ProviderContextInitializer : DropCreateDatabaseIfModelChanges<ProviderContext>
    {
        protected override void Seed(ProviderContext context)
        {
            context.Roles.Add(new IdentityRole() { Id = "1", Name = Models.UserRoles.StudentRole });
            context.Roles.Add(new IdentityRole() { Id = "2", Name = Models.UserRoles.TeacherRole });
            context.Roles.Add(new IdentityRole() { Id = "3", Name = Models.UserRoles.AdminRole });
            context.Roles.Add(new IdentityRole() { Id = "4", Name = Models.UserRoles.SuperUserRole });
            context.SaveChanges();
        }
    }
}