﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServiceProviderApplication.Models
{
    public class Outcome
    {
        public int OutcomeID { get; set; }
        public int ConsumerID { get; set; }
        public string ContextTitle { get; set; }
        public string LisResultSourcedID { get; set; }
        public string ServiceUrl { get; set; }
    }
}
