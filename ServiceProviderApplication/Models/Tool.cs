﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ServiceProviderApplication.Models
{
    public class Tool
    {
        public Tool()
        {
            this.Outcomes = new HashSet<Outcome>();
        }

        public int ToolID { get; set; }

        [Required]
        public string Name { get; set; }

        [DataType(DataType.Html)]
        public string Description { get; set; }

        [DataType(DataType.Html)]
        public string Content { get; set; }

        public virtual ICollection<Outcome> Outcomes { get; set; }
    }

    public class ToolUser
    {
        public string ConsumerName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ReturnUrl { get; set; }
        public string Roles { get; set; }
    }

    public class PostScoreModel
    {
        public string ConsumerName { get; set; }
        public string ContextTitle { get; set; }
        public int OutcomeID { get; set; }
        public double? Score { get; set; }
        public int ToolID { get; set; }
    }
}
