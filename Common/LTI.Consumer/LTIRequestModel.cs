﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LTI.Consumer
{
    public enum UserRoles
    {
        Student
    }

    public class LTIRequestModel
    {
        /// <summary>
        /// Request application instance name.
        /// </summary>
        public string ConsumerInstanceName { get; set; }
        /// <summary>
        /// Consumer key.
        /// </summary>
        public string ConsumerKey { get; set; }
        /// <summary>
        /// Consumer secret.
        /// </summary>
        public string ConsumerSecret { get; set; }
        /// <summary>
        /// LTI URL to launch and authenticate.
        /// </summary>
        public Uri LaunchURL { get; set; }

        /// <summary>
        /// CourseID a.k.a ModuleID which needed to be launched.
        /// </summary>
        public string CourseID { get; set; }
        /// <summary>
        /// Name of the course which needed to be launched.
        /// </summary>
        public string CourseName { get; set; }
        /// <summary>
        /// Is the current user is enrolled in the requested course or not.
        /// </summary>
        public bool IsCurrentUserEnrolledInCourse { get; set; }

        /// <summary>
        /// Current UserID, can be StudentID.
        /// </summary>
        public string UserID { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// Current user's emailID
        /// </summary>
        public string UserEmail { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        internal string UserFullName { get { return string.Format("{0} {1}", this.UserFirstName, this.UserLastName).Trim(); } }
        public UserRoles UserRole { get; set; }
        public Dictionary<string,string> CustomParameters { get; set; }
    }
}
