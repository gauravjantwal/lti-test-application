﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LTI.Consumer
{
    public class LisResultSourcedID
    {
        public string CourseID { get; set; }
        public string UserID { get; set; }
    }
}
