﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConsumerApplication.Startup))]
namespace ConsumerApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
