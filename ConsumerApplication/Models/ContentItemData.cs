﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumerApplication.Models
{
    public class ContentItemData
    {
        public int ContentItemToolId { get; set; }
        public int CourseId { get; set; }
    }
}