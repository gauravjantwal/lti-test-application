﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConsumerApplication.Models
{
    public static class UserRoles
    {
        public static string StudentRole = "Student";
        public static string TeacherRole = "Teacher";
        public static string AdminRole = "Admin";
        public static string SuperUserRole = "SuperUser";
    }
}