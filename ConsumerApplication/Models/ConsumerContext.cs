﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ConsumerApplication.Models
{
    public class ConsumerContext : IdentityDbContext<ApplicationUser>
    {
        public ConsumerContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            //Database.SetInitializer<ConsumerContext>(new CreateDatabaseIfNotExists<ConsumerContext>());
            //Database.SetInitializer<ConsumerContext>(new DropCreateDatabaseIfModelChanges<ConsumerContext>());
            //Database.SetInitializer<ConsumerContext>(new DropCreateDatabaseAlways<ConsumerContext>());
            Database.SetInitializer<ConsumerContext>(new ConsumerContextInitializer<ConsumerContext>());
        }

        public static ConsumerContext Create()
        {
            return new ConsumerContext();
        }

        public DbSet<Assignment> Assignments { get; set; }
        //public DbSet<ContentItemTool> ContentItemTools { get; set; }
        public DbSet<Course> Courses { get; set; }
        //public DbSet<Score> Scores { get; set; }
    }

    public class ConsumerContextInitializer<T> : DropCreateDatabaseIfModelChanges<T> where T : IdentityDbContext<ApplicationUser>
    {
        public override void InitializeDatabase(T context)
        {
            base.InitializeDatabase(context);
        }

        protected override void Seed(T context)
        {
            context.Roles.Add(new IdentityRole() { Id = "1", Name = Models.UserRoles.StudentRole });
            context.Roles.Add(new IdentityRole() { Id = "2", Name = Models.UserRoles.TeacherRole });
            context.Roles.Add(new IdentityRole() { Id = "3", Name = Models.UserRoles.AdminRole });
            context.Roles.Add(new IdentityRole() { Id = "4", Name = Models.UserRoles.SuperUserRole });
            context.SaveChanges();
        }
    }
}