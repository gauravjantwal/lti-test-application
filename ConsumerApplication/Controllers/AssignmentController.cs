﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConsumerApplication.Models;

namespace ConsumerApplication.Controllers
{
    public class AssignmentController : BaseController
    {
        private ConsumerContext db = new ConsumerContext();

        // GET: Assignment
        public async Task<ActionResult> Index()
        {
            return View(await db.Assignments.ToListAsync());
        }

        // GET: Assignment/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = await db.Assignments.FindAsync(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // GET: Assignment/Create
        [Authorize(Roles = "Teacher")]
        public ActionResult Create(int CourseID)
        {
            return View(new CreateEditAssignmentModel { CourseID = CourseID });
        }

        // POST: Assignment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Create([Bind(Include = "CourseID,AssignmentId,ConsumerKey,ConsumerSecret,CustomParameters,Description,Name,Url")] CreateEditAssignmentModel model)
        {
            if (ModelState.IsValid)
            {
                var _course = await db.Courses.FindAsync(model.CourseID);
                if (_course == null)
                {
                    return new HttpNotFoundResult();
                }
                db.Assignments.Add(new Assignment
                {
                    Course = _course,
                    ConsumerKey = model.ConsumerKey,
                    ConsumerSecret = model.ConsumerSecret,
                    CustomParameters = model.CustomParameters,
                    Description = model.Description,
                    Name = model.Name,
                    Url = model.Url
                });
                await db.SaveChangesAsync();
                return RedirectToAction("Details", "Course", new { id = model.CourseID });
            }

            return View(model);
        }

        // GET: Assignment/Edit/5
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = await db.Assignments.FindAsync(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // POST: Assignment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Edit([Bind(Include = "AssignmentId,ConsumerKey,ConsumerSecret,CustomParameters,Description,Name,Url")] Assignment assignment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(assignment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(assignment);
        }

        // GET: Assignment/Delete/5
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Assignment assignment = await db.Assignments.FindAsync(id);
            if (assignment == null)
            {
                return HttpNotFound();
            }
            return View(assignment);
        }

        // POST: Assignment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Assignment assignment = await db.Assignments.FindAsync(id);
            db.Assignments.Remove(assignment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Launch(string returnUrl, int id = 0)
        {
            var _assignment = await db.Assignments.FindAsync(id);
            if (_assignment == null)
            {
                return HttpNotFound();
            }

            var model = new LaunchModel
            {
                AssignmentID = id,
                AssignmentTitle = _assignment.Name,
                CourseTitle = _assignment.Course.Name,
                IsLtiLink = _assignment.IsLtiLink,
                ReturnUrl = returnUrl,
                Url = _assignment.Url
            };
            return View(model);
        }

        /// <summary>
        /// Form a basic LTI launch request for the browser to POST.
        /// </summary>
        /// <param name="id">The assignment ID to launch.</param>
        /// <returns>A form post for the browser to execute.</returns>
        public async Task<ViewResult> LaunchLTI(int id = 0)
        {
            var assignment = await db.Assignments.FindAsync(id);
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            var _model = LTI.Consumer.LTIUtility.CreateBasicLaunchRequestViewModel(Request, new LTI.Consumer.LTIRequestModel()
            {
                ConsumerInstanceName = "PFX",
                ConsumerKey = assignment.ConsumerKey,
                ConsumerSecret = assignment.ConsumerSecret,
                CourseID = assignment.Course.CourseID.ToString(),
                CourseName = assignment.Course.Name,
                IsCurrentUserEnrolledInCourse = assignment.Course.EnrolledUsers.Any(u => u.Id == user.Id),
                LaunchURL = new Uri(assignment.Url),
                UserEmail = user.Email,
                UserFirstName = user.FirstName,
                UserID = user.Id,
                UserLastName = user.LastName,
                UserName = user.UserName,
                UserRole = LTI.Consumer.UserRoles.Student
            });

            //assignment, user

            return View(_model);
        }

        [ChildActionOnly]
        public ViewResult LaunchLTISync(int id = 0)
        {
            var assignment = db.Assignments.Find(id);
            var user = UserManager.FindById(User.Identity.GetUserId());

            var _model = LTI.Consumer.LTIUtility.CreateBasicLaunchRequestViewModel(Request, new LTI.Consumer.LTIRequestModel()
            {
                ConsumerInstanceName = "PFX",
                ConsumerKey = assignment.ConsumerKey,
                ConsumerSecret = assignment.ConsumerSecret,
                CourseID = assignment.Course.CourseID.ToString(),
                CourseName = assignment.Course.Name,
                IsCurrentUserEnrolledInCourse = assignment.Course.EnrolledUsers.Any(u => u.Id == user.Id),
                LaunchURL = new Uri(assignment.Url),
                UserEmail = user.Email,
                UserFirstName = user.FirstName,
                UserID = user.Id,
                UserLastName = user.LastName,
                UserName = user.UserName,
                UserRole = LTI.Consumer.UserRoles.Student
            });

            return View("~/Views/Assignment/LaunchLTI.cshtml", _model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
