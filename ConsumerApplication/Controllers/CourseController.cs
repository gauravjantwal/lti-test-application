﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ConsumerApplication.Models;
using Microsoft.AspNet.Identity;

namespace ConsumerApplication.Controllers
{
    public class CourseController : BaseController
    {
        private ConsumerContext db = new ConsumerContext();

        // GET: Course
        public async Task<ActionResult> Index()
        {
            return View(await db.Courses.ToListAsync());
        }

        // GET: Course/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await db.Courses.FindAsync(id);
            course.Instructor = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (course == null)
            {
                return HttpNotFound();
            }
            if (course.Assignments.Any())
            {
                course.Assignments = course.Assignments.ToList();
            }
            //foreach (var assignment in course.Assignments)
            //{
            //    var scoredAssignment = new ScoredAssignmentModel(assignment);
            //    var score = ConsumerContext.Scores.FirstOrDefault(s =>
            //        s.AssignmentId == assignment.AssignmentId &&
            //        s.UserId == userId);
            //    scoredAssignment.Score = score == null ? null : score.DoubleValue.ToString(CultureInfo.InvariantCulture);
            //    scoredAssignment.UserId = userId;
            //    scoredAssignments.Add(scoredAssignment);
            //}
            return View(course);
        }


        //[ChildActionOnly]
        //public ActionResult ContentItemTools(int courseid)
        //{
        //    var model =
        //        ConsumerContext.ContentItemTools
        //            .Where(t => !string.IsNullOrEmpty(t.Name))
        //            .OrderBy(t => t.Name)
        //            .Select(
        //                t =>
        //                    new ContentItemToolViewModel
        //                    {
        //                        ContentItemToolId = t.ContentItemToolId,
        //                        CourseId = courseid,
        //                        Description = t.Description,
        //                        Name = t.Name
        //                    })
        //            .ToList();
        //    return PartialView("_ContentItemToolsPartial", model);
        //}

        [ChildActionOnly]
        public ActionResult CourseAssignment(int id)
        {
            var userId = User.Identity.GetUserId();
            var assignment = db.Assignments.Find(id);
            //var score = ConsumerContext.Scores.FirstOrDefault(s =>
            //    s.AssignmentId == assignment.AssignmentId &&
            //    s.UserId == userId);
            var model = new ScoredAssignmentModel(assignment)
            {
                Score = null, //score == null ? null : score.DoubleValue.ToString(CultureInfo.InvariantCulture),
                UserID = userId,
            };
            return PartialView("_CourseAssignmentPartial", model);
        }

        //[ChildActionOnly]
        //public ActionResult CourseGradebook(int id)
        //{
        //    var course = ConsumerContext.Courses.Find(id);
        //    var scores = new Dictionary<Tuple<string, int>, string>();
        //    foreach (var user in course.EnrolledUsers)
        //    {
        //        foreach (var assignment in course.Assignments)
        //        {
        //            var score = ConsumerContext.Scores.FirstOrDefault(s =>
        //                s.AssignmentId == assignment.AssignmentId &&
        //                s.UserId == user.Id
        //                );
        //            scores.Add(
        //                new Tuple<string, int>(user.Id, assignment.AssignmentId),
        //                score == null ? null : score.DoubleValue.ToString(CultureInfo.InvariantCulture));
        //        }
        //    }
        //    var model = new CourseGradebookModel
        //    {
        //        Assignments = course.Assignments,
        //        EnrolledUsers = course.EnrolledUsers,
        //        Scores = scores
        //    };
        //    return PartialView("_CourseGradebookPartial", model);
        //}

        //
        // GET: /Course/Enroll
        [ChildActionOnly]
        public ActionResult Enroll(int courseId)
        {
            var course = db.Courses.Find(courseId);
            var enrolled = course.EnrolledUsers.Count(u => u.Id == User.Identity.GetUserId()) > 0;
            var model = new CourseEnrollmentModel
            {
                CourseId = courseId,
                Enrolled = enrolled,
                UserId = User.Identity.GetUserId()
            };
            return PartialView("_EnrollPartial", model);
        }

        //
        // POST: /Course/Enroll/
        [HttpPost]
        public ActionResult Enroll(CourseEnrollmentModel model)
        {
            if (ModelState.IsValid)
            {
                var course = db.Courses.Find(model.CourseId);
                var user = UserManager.FindById(model.UserId);
                if (model.Enrolled)
                {
                    course.EnrolledUsers.Remove(user);
                }
                else
                {
                    course.EnrolledUsers.Add(user);
                }
                db.SaveChanges();
            }
            return RedirectToAction("Details", new { id = model.CourseId });
        }
        // GET: Course/Create
        [Authorize(Roles = "Teacher")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Course/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Teacher")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "CourseId,Name")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Courses.Add(course);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(course);
        }

        // GET: Course/Edit/5
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await db.Courses.FindAsync(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Course/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Edit([Bind(Include = "CourseId,Name")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(course);
        }

        // GET: Course/Delete/5
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = await db.Courses.FindAsync(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Course/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Teacher")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Course course = await db.Courses.FindAsync(id);
            db.Courses.Remove(course);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
