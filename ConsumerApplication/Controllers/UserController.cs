﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ConsumerApplication.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ConsumerApplication.Controllers
{
    public class UserController : Controller
    {
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /User/
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            return View(await UserManager.Users.ToListAsync());
        }

        //
        // GET: /User/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Details(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // GET: /User/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                id = User.Identity.GetUserId();
            }
            var user = await UserManager.FindByIdAsync(id);
            var model = new UserProfileModel(user);
            model.IsStudent = await UserManager.IsInRoleAsync(user.Id, UserRoles.StudentRole);
            model.IsTeacher = await UserManager.IsInRoleAsync(user.Id, UserRoles.TeacherRole);
            return View(model);
        }

        //
        // POST: /User/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(UserProfileModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByIdAsync(model.UserId);
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.SendEmail = model.SendEmail;
                user.SendName = model.SendName;

                await UserManager.UpdateAsync(user);

                await UpdateUserRole(user.Id, UserRoles.StudentRole, model.IsStudent);
                await UpdateUserRole(user.Id, UserRoles.TeacherRole, model.IsTeacher);

                if (User.Identity.GetUserId() == user.Id)
                {
                    var claimsIdentity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                    Request.GetOwinContext().Authentication.SignIn(claimsIdentity);
                }

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        private async Task UpdateUserRole(string userId, string roleName, bool userIsInRole)
        {
            Debug.WriteLine("{0} is in role {1}: {2}", userId, roleName, await UserManager.IsInRoleAsync(userId, roleName));
            if (userIsInRole)
            {
                if (!await UserManager.IsInRoleAsync(userId, roleName))
                    await UserManager.AddToRoleAsync(userId, roleName);
            }
            else
            {
                if (await UserManager.IsInRoleAsync(userId, roleName))
                    await UserManager.RemoveFromRoleAsync(userId, roleName);
            }
        }

        //
        // GET: /User/Delete/5
        [Authorize(Roles = "SuperUser")]
        public async Task<ActionResult> Delete(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /User/Delete/5
        [Authorize(Roles = "SuperUser")]
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var user = await UserManager.FindByIdAsync(id);

            // Remove this user from any roles they may have
            foreach (var role in user.Roles)
            {
                await UserManager.RemoveFromRoleAsync(user.Id, role.ToString());
            }
            // Remove the user's account
            var _result = await UserManager.DeleteAsync(user);

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> HeartBeat()
        {
            return await Task.FromResult<ContentResult>(Content(User.Identity.IsAuthenticated.ToString()));
        }
    }
}